import art

alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
alphabet_length = len(alphabet) - 1
cipher = ""
keep_Using = True

def caesar(text, shift,direction):
   
    global cipher
    if direction == "decode":
        shift *= -1
    
    for letter in text:
        if letter not in alphabet:
            cipher += letter
        else:   
            original_position = alphabet.index(letter)
            cipher_position = original_position + shift
            remaining_alphabet_index = alphabet_length - original_position

            if cipher_position > alphabet_length:
                cipher_position = cipher_position - remaining_alphabet_index - original_position - 1

            cipher += alphabet[cipher_position]

def check_shift_range(text, shift,direction):
    global cipher
    if shift > alphabet_length + 1:
        shift = shift % 26
        caesar (text,shift,direction)  

print(art.logo)

def keep_using_question():
    global keep_Using
    answer = int(input ("Keep Using Cipher?\n1)Yes            2)No\n"))
    if answer == 2:
        keep_Using = False

while keep_Using:
    direction = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n")
    text = input("Type your message:\n").lower()
    shift = int(input("Type the shift number:\n"))
    check_shift_range(text,shift,direction)
    print (cipher)
    cipher = ""
    keep_using_question()




